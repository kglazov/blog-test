<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'post' => 'required|min:2',
        ];
    }

    /**
     * Get error response in case validation failed.
     *
     * @param array $errors
     * @return json
     */
    public function response(array $errors)
    {
        return response()->json([
            'errors' => $errors
        ]);
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Name is required!',
            'post.required' => 'Post is required!'
        ];
    }
}
