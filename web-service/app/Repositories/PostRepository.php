<?php

namespace App\Repositories;

use App\Models\Post;
use App\User;
use App\Repositories\Interfaces\PostRepositoryInterface;

class PostRepository implements PostRepositoryInterface
{
    public function all()
    {
        return Post::all();
        // return Blog::orderBy('id', 'desc')->paginate(5);
    }

    public function getPost($id) 
    {
        return Post::findOrFail($id);
    }

    public function getByUser(User $user)
    {
        //return Blog::where('user_id'. $user->id)->get();
    }
}
